Configuração do processo de captura de Projetos de Lei
======================================================

Depois que o ambiente básico de desenvolvimento está configurado, basta seguir
as instruções abaixo para configurar o processo que captura as informações de
Projetos de Lei dos sites da Câmara e do Senado.

Se você ainda não configurou o ambiente básico, por favor o faça seguindo [as
instruções][contributing].


Download
--------

O comando `python radarlegislativo/manage.py fetch_projeto` permite
fazer o download de projetos diretamente pela linha de comando (desde
que o celery esteja em execução). Para isso, é necessário informar a origem do
projeto e o id dele no site correspondente. Um exemplo é:

```
python radarlegislativo/manage.py fetch_projeto --origem camara --id 548066
```

Esse comando vai baixar do site da Câmara o projeto com id `548066`.

Se você precisa baixar vários projetos de uma só vez, use a opção `--from-file`
(informando um arquivo no mesmo formato que [projetos-de-leis.yml][pls-yml]).
Por exemplo:

```
python radarlegislativo/manage.py fetch_projeto --from-file projetos-de-leis.yml
```

Problemas comuns
----------------

### `ConnectionError` / `Connection refused`

Se você se deparar com um erro parecido com o seguinte:

```
[2018-02-01 18:02:21,021: ERROR/ForkPoolWorker-1] celery_haystack.tasks.CeleryHaystackSignalHandler[2d83a5a6-c344-4a32-9f1e-c2f678b7994f]: ConnectionError(<urllib3.connec
tion.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused) caused by: NewConnectionError(<urllib3.connection.HT$PConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused)
Traceback (most recent call last):
    [...]
ConnectionError: ConnectionError(<urllib3.connection.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused) caus$
d by: NewConnectionError(<urllib3.connection.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused)
```

É possível que o elasticsearch não esteja respondendo na porta 9200. Verifique
se ele está em execução (o comando `curl http://localhost:9200/` deve retornar
informações sobre a instalação do elasticsearch).

Se o problema permanecer, verifique sua conexão com a internet.



[contributing]: https://gitlab.com/codingrights/radarlegislativo/blob/master/CONTRIBUTING.md
[pls-yml]: https://gitlab.com/codingrights/radarlegislativo/blob/master/projetos-de-leis.yml
