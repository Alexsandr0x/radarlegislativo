#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"
PARENT_DIR="$(dirname $SCRIPT_DIR)"

echo "Starting elasticsearch..."
"$SCRIPT_DIR/run_elasticsearch.sh" &
ELASTIC_PID=$!
echo "PID do elasticsearch: $ELASTIC_PID"

echo "Starting celery..."
cd "$PARENT_DIR"
make dev_celery &
CELERY_PID=$!
echo "PID do elasticsearch: $CELERY_PID"

echo "Starting webserver..."
cd "$PARENT_DIR"
make dev_server &
SERVER_PID=$!
echo "PID do servidor: $SERVER_PID"

trap "kill 0; exit" SIGINT SIGTERM SIGKILL

while :
do
    sleep 1
done
