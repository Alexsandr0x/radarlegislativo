# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

Esse script usa projetos de lei específicos para verificar se o
fetcher se comporta da maneira esperada.

Ele não é incluído como um teste por uma série de motivos mas,
principalmente, por que faz requisições para os sites da Câmara e do
Senado e não é estável (as informações do PL podem mudar).

A ideia é que esse script seja executado por um desenvolvedor quando
for necessário checar se o fetcher está buscando as informações de
maneira correta e sua saída seja comparada manualmente com a página
dos projetos em questão.

O jeito mais simples de executar esse script é usando o shell do
django (`python manage.py shell -c 'import main.check_fetcher'`)

"""

import datetime

from .fetcher import fetch_camara_project, fetch_senado_project
from .models import Projeto
from .models import URL_WEB_CAMARA, URL_WEB_SENADO


def check(expected_values, downloader):
    id_site = expected_values['id_site']
    projeto = downloader(id_site, save=False)
    for field, expected in expected_values.items():
        result = getattr(projeto, field)
        if field == "apresentacao":
            result = projeto.apresentacao.date()
        assert result == expected, result
        print(result)

    print("\n\nTodos os campos estão como o esperado \o/")


def check_camara(expected_values):
    print(URL_WEB_CAMARA.format(expected_values['id_site']))
    check(expected_values, fetch_camara_project)


def check_senado(expected_values):
    print(URL_WEB_SENADO.format(expected_values['id_site']))
    check(expected_values, fetch_senado_project)


######

PL_548066 = {
    "origem": Projeto.CAMARA,
    "id_site": 548066,
    "nome": u"PL 4060/2012",
    "apresentacao": datetime.date(2012, 6, 13),
    "ementa": u"Dispõe sobre o tratamento de dados pessoais, e dá outras providências.",
    "autoria": u"Milton Monti",
    "apensadas": u"PL 5276/2016, PL 6291/2016",
    "local": u'Comissão Parecer Comissão Especial destinada a proferir parecer ao Projeto de Lei nº 4060, de 2012, do Dep. Milton Monti, que "dispõe sobre o tratamento de dados pessoais e dá outras providências", e apensado   ( PL406012 ) -',
}

check_camara(PL_548066)

print("-----------------------------")
PL_91221 = {
    "origem": Projeto.SENADO,
    "id_site": 91221,
    "nome": u"PLC 85",
    "apresentacao": datetime.date(2009, 5, 22),
    "ementa": u"Disciplina o funcionamento de bancos de dados e serviços de proteção ao crédito e congêneres e dá outras providências.",
    "autoria": u"DEPUTADO Bernardo Ariston",
    "apensadas": u"PLS 55, PLS 209",
    "local": u"Comissão de Transparência, Governança, Fiscalização e Controle e Defesa do Consumidor",
}

check_senado(PL_91221)
