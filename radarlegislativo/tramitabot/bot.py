# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2017 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import textwrap
import sys
from django.conf import settings
from django.template.loader import render_to_string
from django.template.defaultfilters import truncatechars
import telepot

from main.models import Tramitacao
from .models import TramitabotUser

HELP_TEXT = (
u"""
Esse bot envia notificações de atualizações nos projetos de lei que são relevantes para a privacidade, liberdade de expressão, acesso e questões de gênero no meio digital. A lista  completa dos projetos atualmente monitorados está em https://codingrights.gitlab.io/pls/.

Esta é a versão beta, logo teremos mais funcionalidades e comandos implementados. Se você tem sugestões, também pode entrar em contato pelo email contact@codingrights.org

Use os seguintes comandos para interagir com o bot:

/cadastrar - Para passar a receber notificações.
/descadastrar - Para deixar de receber notificações.
/ajuda - Para mostrar esse texto.
/ultimas - Para mostrar últimas tramitações (você pode, se quiser, informar o número de tramitações que quer ver. Por exemplo: /ultimas 5).


""")


class Tramitabot(telepot.Bot):

    handlers = {
        "/start": "print_help",
        "/ajuda": "print_help",
        "/help": "print_help",
        "/ultimas": "ultimas",
        "/últimas": "ultimas",
        "/cadastrar": "register_user",
        "/descadastrar": "unregister_user",
    }

    help_text = HELP_TEXT
    registered_message = (u"A partir de agora você vai receber notificações "
                          u"regulares com as últimas tramitações.")
    default = "default_handler"
    template_name = "tramitabot/ultimas_tramitacoes.txt"
    chars_limit = 4096

    def __init__(self):
        return super(Tramitabot, self).__init__(settings.TRAMITABOT_API_TOKEN)

    def print_help(self, msg):
        return self.help_text

    def default_handler(self, msg):
        return textwrap.dedent(u"""
        Não entendi...
        /ajuda mostra todos os comandos válidos :)
        """)

    def register_user(self, msg):
        user_info = {
            "username": msg["from"].get("username", ""),
            "first_name": msg["from"].get("first_name", ""),
            "last_name": msg["from"].get("last_name", ""),
            "telegram_id": msg["from"]["id"],
            "language_code": msg["from"].get("language_code", ""),
        }
        user, created = TramitabotUser.objects.get_or_create(**user_info)
        return self.registered_message

    def unregister_user(self, msg):
        telegram_id = msg["from"]["id"]
        try:
            user = TramitabotUser.objects.get(telegram_id=telegram_id)
            user.delete()
        except TramitabotUser.DoesNotExist:
            pass
        return (u"A partir de agora você não vai mais receber "
                u"notificações automáticas desse bot.")

    def get_ultimas_tramitacoes(self, n):
        reply_text = render_to_string(self.template_name,
                {"tramitacoes": Tramitacao.objects.order_by("-data")[:n]})
        return truncatechars(reply_text, self.chars_limit)

    def ultimas(self, msg):
        text = msg["text"]
        splitted_text = text.split()
        if len(splitted_text) == 1:
            n = 15
        else:
            try:
                n = int(splitted_text[1].strip())
            except ValueError:
                return textwrap.dedent(u"""
                Esse comando deve ser usado de uma das seguintes formas:
                /ultimas
                /ultimas _n_ (onde _n_ é um número entre 1 e 20)
                """)
            if n > 20 or n <= 0:
                return u"O número de tramitações deve ser entre 1 e 20."
        return self.get_ultimas_tramitacoes(n)

    def handle_messages(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        if content_type == "text":
            text = msg["text"]
            command = text.split()[0]

            handler = getattr(self, self.handlers.get(command,
                self.default))
            reply = truncatechars(handler(msg), self.chars_limit)

            user_id = msg["from"]["id"]

            self.sendMessage(user_id, reply, parse_mode="Markdown",
                    disable_web_page_preview=True)

    def send_updates(self):
        msg = self.get_ultimas_tramitacoes(15)
        for user in TramitabotUser.objects.filter(has_blocked_us=False):
            try:
                self.sendMessage(user.telegram_id, msg, parse_mode="Markdown",
                                 disable_web_page_preview=True)
            except telepot.exception.BotWasBlockedError:
                sys.stderr.write("User {} has blocked tramitabot\n".format(user))
                user.has_blocked_us = True
                user.save()
            except telepot.exception.TelegramError as exc:
                sys.stderr.write("There was an error while sending updates "
                                 "to user {}: {}\n".format(user, exc))

    def listen(self):
        return self.message_loop(self.handle_messages,
                run_forever=u"Tramitabot esta esperando mensagens...")
