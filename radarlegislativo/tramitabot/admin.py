# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import TramitabotUser

class TramitabotUserAdmin(admin.ModelAdmin):
    list_display = ("username", "first_name", "last_name", "telegram_id", "has_blocked_us")
    list_filter = ("has_blocked_us", )

admin.site.register(TramitabotUser, TramitabotUserAdmin)
